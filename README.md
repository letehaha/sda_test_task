# How to use

Simply clone, download all dependencies using `npm i` command and start the project via `npm run dev`


**PS:** I'm not sure how to add support for TypeScript types both in the web and node envs. If tsconfig's `module: commonjs` - React working only with `import * as React from 'react'` syntax. If `module: esnext` - NodeJS imports not working. It seems like we need to have two different configs, or types might be added in some another way, but I haven't had time to investigate this.

**PSS:** As I understood right, the main requirement is to just display posts using required tools and services. Add/edit/delete post not working, because I didn't get this from requirements.
