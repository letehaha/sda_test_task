const express = require('express');
const jwt = require('jsonwebtoken');

const userMock = require('../../mocks/user.json');
const postsMock = require('../../mocks/posts.json');

const app = express();

app.use(express.static('dist'));
app.use(express.json());

app.post('/api/login', (req: any, res: any) => {
  if (req.body.username === userMock.username && req.body.password === userMock.password) {
    const token = jwt.sign({ id: userMock.id }, process.env.JWT_KEY);
    userMock.token = token;
    res.send({ token });
  } else {
    res.status(404).send({ error: 'User does not exist' })
  }
});

app.get('/api/posts', (req: any, res: any) => {
  const token = req.header('Authorization').replace('Bearer ', '');
  // we need to add typings here
  const data: any = jwt.verify(token, process.env.JWT_KEY);
  try {
    if (userMock.id === data.id && userMock.token === token) {
      res.send(postsMock.filter((post: any) => +post.userId === +userMock.id));
    } else {
      throw new Error('User not found');
    }
  } catch (error) {
    res.status(401).send({ error: 'Not authorized to access this resource' });
  }
});

app.listen(process.env.PORT || 8080, () => console.log(`Listening on port ${process.env.PORT || 8080}!`));
