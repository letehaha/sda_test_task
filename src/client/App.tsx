import React, { Component } from 'react';
import './app.css';
import userMock from '../../mocks/user.json';
import { PostInterface, UserInterface } from '../interfaces';

interface State {
  posts: PostInterface[];
  username: string;
  password: string;
  isDataLoaded: boolean;
};

export default class App extends Component<{}, State> {
  constructor(props: {}) {
    super(props);
    this.state = {
      username: 'test',
      password: 'task',
      isDataLoaded: false,
      posts: [],
    };
  }

  async componentDidMount() {
    const { username, password } = this.state;
    const { token } = await (
      await fetch('/api/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ username, password })
      })
    ).json();
    const posts: PostInterface[] = await (
      await fetch('/api/posts', {
        method: 'GET',
        headers: {
          Authorization: `Bearer ${token}`,
        }
      })
    ).json();
    this.setState({ isDataLoaded: true, posts });
  }

  render() {
    const { username, isDataLoaded, posts } = this.state;

    let content = null;
    if (isDataLoaded) {
      content = (
        <div className="posts">
          {posts.map(post => (
            <div className="posts__item post" key={post.id}>
              <h2 className="post__title">{post.title}</h2>
              <p className="post__descr">{post.body}</p>
            </div>
          ))}
        </div>
      );
    }
    return (
      <main>
        <h1 className="main-title">
          {isDataLoaded ? `Hello "${username}"!` : 'Loading.. please wait!'}
        </h1>
        {content}
      </main>
    );
  }
}
